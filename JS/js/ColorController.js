var app = angular
.module('ColorApp', ['ngRoute', 'mp.colorPicker'])
.config(function($routeProvider, $locationProvider) {
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
});

app.controller('ColorController', ['$scope', function($scope) {
    $scope.settings = null;
    $scope.selectedComponent = null;
    $scope.selectedStyle = null;
    $scope.votesText = null;
    $scope.selectedTab = null;

    $scope.randomPercentage = Math.floor(Math.min(Math.random() * 100));
    $scope.randomVotecount =  Math.round(Math.max(5, (parseFloat($scope.randomPercentage)/ 4)));

    $scope.fontTypes = [
        // ClassName / Font Family
        ['calibri', 'Calibri'],
        ['arial', 'Arial'],
        ['times', 'Times New Roman'],
        ['courier', 'Courier New']         
    ];

    $scope.updateFontSize = (newFontSize) => {
        let newTab =  {...$scope.selectedTab};
        let newSettings = [...$scope.settings];

        if($scope.selectedComponent === 'votesText') {
            let newVotesText = {...$scope.votesText};
            newVotesText.fontSize = newFontSize;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fontSize = newFontSize;
            newSettings.forEach(tab => {
                if(tab.index ===  $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    }

    $scope.updateFontFamily = (newFontFamily) => {
        let newTab =  {...$scope.selectedTab};
        let newSettings = [...$scope.settings];

        if($scope.selectedComponent === 'votesText') {
            let newVotesText = {...$scope.votesText};
            newVotesText.fontFamily = newFontFamily;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fontFamily = newFontFamily;
            newSettings.forEach(tab => {
                if(tab.index ===  $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    }

    $scope.updateFillColor = (newColor) => {
        let newTab =  {...$scope.selectedTab};
        let newSettings = [...$scope.settings];

        if($scope.selectedComponent === 'votesText') {
            let newVotesText = {...$scope.votesText};
            newVotesText.fillColor = newColor;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fillColor = newColor;
            newSettings.forEach(tab => {
                if(tab.index ===  $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    }

    $scope.updateFontColor = (newColor) => {
        let newTab =  {...$scope.selectedTab};
        let newSettings = [...$scope.settings];

        if($scope.selectedComponent === 'votesText') {
            let newVotesText = {...$scope.votesText};
            newVotesText.fontColor = newColor;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fontColor = newColor;
            newSettings.forEach(tab => {
                if(tab.index ===  $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    }

    $scope.selectVotes = component => {
        $scope.selectedComponent = component;
        $scope.selectedStyle = Object.keys($scope.votesText);
        $scope.generateRandomPercentage();
    }

    $scope.selectComponent = component => {
        $scope.selectedComponent = component;
        $scope.selectedStyle = Object.keys($scope.selectedTab[component]);
        $scope.generateRandomPercentage();
    }

    $scope.setSettings = (settings) => {
        $scope.settings = settings;
        $scope.updatePageTag();
    }

    $scope.setVotesText = (votesText) => {
        $scope.votesText = votesText;
        $scope.selectedStyle = Object.keys(votesText);
        $scope.updatePageTag();
    }

    $scope.updatePageTag = () => {
        let merged = {
            settings: {
                ...$scope.settings
            },
            votesText: {
                ...$scope.votesText
            } 
        };
        document.getElementById('jsonTag').innerHTML = merged;
        
    }

    $scope.selectTab = (tabIndex) => {
        $scope.settings.forEach(tab => {
            if(tab.index === tabIndex) {
                $scope.selectedTab = tab;
            }
        })
    }

    $scope.drawHtml = (jsonSettings) => {
        if(jsonSettings) {
            if(!jsonSettings.settings || typeof jsonSettings.settings !== 'object') {
                throw "No settings object passed!";
            }
            if(!jsonSettings.votesText || typeof jsonSettings.votesText !== 'object') {
                throw "No votesText object passed!";
            }
            $scope.setVotesText(jsonSettings.votesText);
            $scope.setSettings(jsonSettings.settings);
            $scope.selectTab(jsonSettings.settings[0].index);
            $scope.$apply();
        }
    }

    $scope.generateRandomPercentage = () => {
        $scope.randomPercentage = Math.floor(Math.min(Math.random() * 100));
        $scope.randomVotecount =  Math.round(Math.max(5, (parseFloat($scope.randomPercentage)/ 4)));
    }
  }]);
