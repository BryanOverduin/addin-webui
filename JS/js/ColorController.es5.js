'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
var app = angular.module('ColorApp', ['ngRoute', 'mp.colorPicker']).config(function ($routeProvider, $locationProvider) {
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
});

app.controller('ColorController', ['$scope', function ($scope) {
    $scope.settings = null;
    $scope.selectedComponent = null;
    $scope.selectedStyle = null;
    $scope.votesText = null;
    $scope.selectedTab = null;
    $scope.randomPercentage = Math.floor(Math.min(Math.random() * 100));
    $scope.randomVotecount = Math.round(Math.max(5, parseFloat($scope.randomPercentage) / 4));
    $scope.fontTypes = [
    // ClassName / Font Family
    ['calibri', 'Calibri'], ['arial', 'Arial'], ['times', 'Times New Roman'], ['courier', 'Courier New']];

    $scope.updateFontSize = function (newFontSize) {
        var newTab = _extends({}, $scope.selectedTab);
        var newSettings = [].concat($scope.settings);

        if ($scope.selectedComponent === 'votesText') {
            var newVotesText = _extends({}, $scope.votesText);
            newVotesText.fontSize = newFontSize;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fontSize = newFontSize;
            newSettings.forEach(function (tab) {
                if (tab.index === $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    };

    $scope.updateFontFamily = function (newFontFamily) {
        var newTab = _extends({}, $scope.selectedTab);
        var newSettings = [].concat($scope.settings);

        if ($scope.selectedComponent === 'votesText') {
            var newVotesText = _extends({}, $scope.votesText);
            newVotesText.fontFamily = newFontFamily;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fontFamily = newFontFamily;
            newSettings.forEach(function (tab) {
                if (tab.index === $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    };

    $scope.updateFillColor = function (newColor) {
        var newTab = _extends({}, $scope.selectedTab);
        var newSettings = [].concat($scope.settings);

        if ($scope.selectedComponent === 'votesText') {
            var newVotesText = _extends({}, $scope.votesText);
            newVotesText.fillColor = newColor;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fillColor = newColor;
            newSettings.forEach(function (tab) {
                if (tab.index === $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    };

    $scope.updateFontColor = function (newColor) {
        var newTab = _extends({}, $scope.selectedTab);
        var newSettings = [].concat($scope.settings);

        if ($scope.selectedComponent === 'votesText') {
            var newVotesText = _extends({}, $scope.votesText);
            newVotesText.fontColor = newColor;
            $scope.setVotesText(newVotesText);
        } else {
            newTab[$scope.selectedComponent].fontColor = newColor;
            newSettings.forEach(function (tab) {
                if (tab.index === $scope.selectedTab.index) {
                    tab = newTab;
                    $scope.setSettings(newSettings);
                }
            });
        }
    };

    $scope.pollStyles = function () {
        var jsonTag = document.getElementById("jsonTag");
        
        if (jsonTag && jsonTag.innerHTML) {
            var stringified = JSON.parse(jsonTag.innerHTML);
            $scope.drawHtml(stringified);
        } else {
            setTimeout(function () {
                document.getElementById("loaded").innerHTML = true;
                $scope.pollStyles();
            }, 500);
        }
    };

    $scope.selectVotes = function (component) {
        $scope.selectedComponent = component;
        $scope.selectedStyle = Object.keys($scope.votesText);
        $scope.generateRandomPercentage();
    };

    $scope.selectComponent = function (component) {
        $scope.selectedComponent = component;
        $scope.selectedStyle = Object.keys($scope.selectedTab[component]);
        $scope.generateRandomPercentage();
    };

    $scope.setSettings = function (settings) {
        $scope.settings = settings;
        $scope.updatePageTag();
    };

    $scope.setVotesText = function (votesText) {
        $scope.votesText = votesText;
        $scope.selectedStyle = Object.keys(votesText);
        $scope.updatePageTag();
    };

    $scope.updatePageTag = function () {
        var merged = {
            settings: _extends({}, $scope.settings),
            votesText: _extends({}, $scope.votesText)
        };
        document.getElementById('jsonTag').innerHTML = merged;
    };

    $scope.selectTab = function (tabIndex) {
        $scope.settings.forEach(function (tab) {
            if (tab.index === tabIndex) {
                $scope.selectedTab = tab;
            }
        });
    };

    $scope.drawHtml = function (jsonSettings) {
        if(jsonSettings) {
            if(!jsonSettings.settings || typeof jsonSettings.settings !== 'object') {
                throw "No settings object passed!";
            }
            if(!jsonSettings.votesText || typeof jsonSettings.votesText !== 'object') {
                throw "No votesText object passed!";
            }
            $scope.setVotesText(jsonSettings.votesText);
            $scope.setSettings(jsonSettings.settings);
            $scope.selectTab(jsonSettings.settings[0].index);
            $scope.$apply();
        }
    };

    $scope.generateRandomPercentage = function () {
        $scope.randomPercentage = Math.floor(Math.min(Math.random() * 100));
        $scope.randomVotecount = Math.round(Math.max(5, parseFloat($scope.randomPercentage) / 4));
    };
}]);