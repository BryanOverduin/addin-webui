
export default function tabsReducer(state, action) {
    switch(action.type) {
        case 'SELECT_STYLE': {
            return {
                ...state,
                selectedStyleName: action.selectedStyleName,
                selectedStyle: action.selectedStyle,
                randomPercentage: Math.floor(Math.random() * 100) + '%'
            }
        }
        case 'SET_SETTINGS': {
            let allTabs = action.initialSettings.settings;
            return {
                ...state,
                allTabs: action.initialSettings.settings,
                selectedTab: allTabs[0],
                votesText: action.initialSettings.votesText
            }
        }
        case 'SELECT_TAB': {
            let currentTab = state.selectedTab;
            let newCurrentTab = state.selectedTab;
            let newTabs = state.allTabs;
            newTabs.forEach((tab, index) => {
                let tabIndex = tab.index;
                if(tabIndex === currentTab.index) {
                    newTabs[index] = currentTab;
                }
                if(tabIndex === action.tabId) {
                    newCurrentTab = tab;
                }
            })
            return {
                ...state,
                allTabs: newTabs,
                selectedTab: newCurrentTab,
                randomPercentage: Math.floor(Math.random() * 100) + '%'
            }
        }
        case 'UPDATE_TAB': {
            let currentTab = action.newTabSettings;
            let newTabs = state.allTabs;
            newTabs.forEach((tab, index) => {
                let tabIndex = tab.index;
                if(tabIndex === currentTab.index) {
                    newTabs[index] = currentTab;
                }
            })
            return {
                ...state,
                selectedTab: currentTab,
                allTabs: newTabs
            }
        }
        case 'UPDATE_VOTES_TEXT': {
            return {
                ...state,
                votesText: action.votesText
            }
        }
        default: {
            return {
                ...state
            }
        }
    }
}