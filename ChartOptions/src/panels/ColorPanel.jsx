import React, { Component } from 'react';
import ColorItem from './components/ColorItem';
import BulletSelect from './components/select/BulletSelect';
import BarSelect from './components/select/BarSelect';
import PercentageSelect from './components/select/PercentageSelect';
import VotesSelect from './components/select/VotesSelect';
import AnswerSelect from './components/select/AnswerSelect';
import TabsSelect from './components/tab/TabsSelect';
import { connect } from 'react-redux';
import { loadSettings } from '../actions/tabsActions';


class ColorPanel extends Component {

    componentWillMount() {
        this.pollStyles();   
    }

    componentWillReceiveProps(nextProps) {
        let stringified = JSON.stringify(nextProps.allTabs);
        document.getElementById("jsonTag").innerHTML = stringified;
    }

    pollStyles() {
        let jsonSettings = document.getElementById("jsonTag").innerHTML;
        if(jsonSettings) {
            if(this.validateSettings(jsonSettings)) {
                let stringified = JSON.parse(jsonSettings);
                this.drawHtml(stringified);
            }
        } else {
            setTimeout(() => {
                this.pollStyles();
            }, 500);
        }
    }

    validateSettings(jsonSettings) {
        return true;
    }

    drawHtml(jsonSettings) {
        this.props.dispatch(loadSettings(jsonSettings));
    }

    saveSettingsBtnClick() {
        let stringifiedSettings = JSON.stringify(this.props.allTabs);
        console.log(stringifiedSettings)
        document.getElementById("jsonTag").innerHTML = stringifiedSettings;
        window.location.href = 'about:save';
    }

    render() {

        if(this.props.selectedTab) {
            return (
                <div className="wrapper">
                    <div className="holder">
                        <div className="color-panel">
                            <div className="color-panel-content color-items">
                                <ColorItem selectedStyle={this.props.selectedStyle} />
                            </div>
                            <div className="tabs">
                                <TabsSelect />
                            </div>
                            <div className="container-fluid select-panel">
                                <div className="row full-height">
                                    <BulletSelect />
                                    <AnswerSelect />
                                    <BarSelect />
                                    <PercentageSelect />
                                    <VotesSelect />
                                </div>
                            </div>
                        
                            <div className="bottom-button" onClick={this.saveSettingsBtnClick.bind(this)} >
                                <p>Save settings</p>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="wrapper">
                <div className="holder">
                    <div className="color-panel">
                        <div className="empty-styles text-center">
                            <h3><i>Call the drawHtml(JSONSettings) function with specified arguments to load data </i></h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    (state) => {
        return {
            allTabs: state.tabsReducer.allTabs,
            selectedTab: state.tabsReducer.selectedTab,
            selectedStyle: state.tabsReducer.selectedStyle,
            votesText: state.tabsReducer.votesText
        }
    }
) (ColorPanel);