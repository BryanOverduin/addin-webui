import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectStyle } from '../../../actions/tabsActions';


class AnswerSelect extends Component {

    constructor(props) {
        super(props);
        this.name = 'answerStyle';
    }

    getSelected() {
        if(this.props.selectedStyleName) {
            return this.name === this.props.selectedStyleName && 'selected';
        }
    }

    select() {
        let answerStyle = this.props.selectedTab.answerStyle;
        this.props.dispatch(
            selectStyle(
                answerStyle,
                this.name
            )
        );
    }

    render() {
        let style = this.props.selectedTab.answerStyle;
        let styles = {
            fontSize: parseInt(style.fontSize, 10),
            fontFamily: style.fontFamily,
            color: style.fontColor
        }
        return (
            <div style={styles} onClick={this.select.bind(this)} className={"col-md-3 col-lg-3 col-xs-3 col-sm-3 full-height padding-0-10"}>
                <div className={this.getSelected() + " panel panel-default full-height vertical-align-wrap height-100px "}>
                    <div className="vertical-align vertical-align--middle">
                        <h4 style={styles} className="example-answer">Answer</h4>
                    </div>
                </div>
            </div>
        )
        
    }
}
export default connect(
    (state) => {
        return {
            selectedTab: state.tabsReducer.selectedTab,
            selectedStyleName: state.tabsReducer.selectedStyleName
        }
    }
)(AnswerSelect);