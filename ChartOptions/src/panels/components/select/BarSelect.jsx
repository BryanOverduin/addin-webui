import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectStyle } from '../../../actions/tabsActions';

class BarSelect extends Component {

    constructor(props) {
        super(props) 
        this.name = 'barStyle';
    }

    select() {
        let barStyle = this.props.selectedTab.barStyle;
        this.props.dispatch(
            selectStyle(
                barStyle,
                this.name
            )
        );
    }

    getSelected() {
        if(this.props.selectedStyleName) {
            return this.name === this.props.selectedStyleName && 'selected';
        }
    }

    render() {
        let style = this.props.selectedTab.barStyle;
        let styles = {
            backgroundColor: style.fillColor,
            width: this.props.randomPercentage
        }
        return (
            <div onClick={this.select.bind(this)} className="col-md-5 col-lg-5 col-xs-5 col-sm-5 full-height padding-0-10">
                <div className={this.getSelected() + " panel panel-default full-height vertical-align-wrap height-100px"}>
                    <div className="vertical-align vertical-align--middle">
                        <div style={styles} className="example-bar">
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    (state) => {
        return {
            selectedTab: state.tabsReducer.selectedTab,
            selectedStyleName: state.tabsReducer.selectedStyleName,
            randomPercentage: state.tabsReducer.randomPercentage
        }
    }
)(BarSelect);