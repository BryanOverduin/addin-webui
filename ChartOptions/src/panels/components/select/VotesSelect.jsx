import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectStyle } from '../../../actions/tabsActions';

class VotesSelect extends Component {

    constructor(props) {
        super(props) 
        this.name = 'votesText';
    }
    
    getSelected() {
        if(this.props.selectedStyleName) {
            return this.name === this.props.selectedStyleName && 'selected';
        }
    }

    select() {
        let votesText = this.props.votesText;
        this.props.dispatch(
            selectStyle(
                votesText,
                this.name
            )
        );
    }

    render() {
        let randomVotecount = Math.round(Math.max(5, (parseFloat(this.props.randomPercentage)/ 4)));
        let style = this.props.votesText;
        let styles = {
            fontSize: parseInt(style.fontSize || 10, 10),
            color: style.fontColor,
            fontFamily: style.fontFamily
        }

        return (
            <div style={styles} onClick={this.select.bind(this)} className="col-md-4 pull-right padding-0-10">
                <div className={this.getSelected() + " panel panel-default full-height vertical-align-wrap height-100px"}>
                    <div className="vertical-align vertical-align--middle">
                        <h4 style={styles} className="example-percentage">
                            <span style={{background: styles.color}} className="vote-count">{randomVotecount}</span> Votes
                        </h4>
                    </div>
                </div>
            </div>
        )
    }
}
export default connect(
    (state) => {
        return {
            selectedTab: state.tabsReducer.selectedTab,
            selectedStyleName: state.tabsReducer.selectedStyleName,
            votesText: state.tabsReducer.votesText,
            randomPercentage: state.tabsReducer.randomPercentage
        }
    }
)(VotesSelect);