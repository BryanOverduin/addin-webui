import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectStyle } from '../../../actions/tabsActions';


class PercentageSelect extends Component {

    constructor(props) {
        super(props) 

        this.name = 'percentageStyle';
    }

    select() {
        let percentageStyle = this.props.selectedTab.percentageStyle;
        this.props.dispatch(
            selectStyle(
                percentageStyle,
                this.name
            )
        );
    }

    getSelected() {
        if(this.props.selectedStyleName) {
            return this.name === this.props.selectedStyleName && 'selected';
        }
    }

    render() {

        let style = this.props.selectedTab.percentageStyle;
        let styles = {
            fontSize: parseInt(style.fontSize, 10),
            fontFamily: style.fontFamily,
            color: style.fontColor
        }

        return (
            <div style={styles} onClick={this.select.bind(this)} className="col-md-2 col-lg-2 col-xs-2 col-sm-2 full-height padding-0-10">
                <div className={this.getSelected() + " panel panel-default full-height vertical-align-wrap height-100px"}>
                    <div className="vertical-align vertical-align--middle">
                        <h4 style={styles} className="example-percentage">{this.props.randomPercentage}</h4>
                    </div>
                </div>
            </div>
        )
        
    }
}
export default connect(
    (state) => {
        return {
            selectedTab: state.tabsReducer.selectedTab,
            selectedStyleName: state.tabsReducer.selectedStyleName,
            randomPercentage: state.tabsReducer.randomPercentage
        }
    }
)(PercentageSelect);