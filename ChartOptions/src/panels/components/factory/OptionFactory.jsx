import React, { Component } from 'react';
import OptionFontSize from '../input/OptionFontSize';
import OptionFontColor from '../input/OptionFontColor';
import OptionFontFamily from '../input/OptionFontFamily';
import OptionFillColor from '../input/OptionFillColor';


class OptionFactory extends Component {

    getOptions(selectedStyle) {
        let optionListComponent = [];

        if(!selectedStyle) {
            return optionListComponent;
        }

        if(selectedStyle.hasOwnProperty('fontColor')) {
            optionListComponent.push(
                <OptionFontColor 
                    key={0} 
                />
            );
        }
        if(selectedStyle.hasOwnProperty('fontSize')) {
            optionListComponent.push(
                <OptionFontSize 
                    key={1} 
                />
            );
        }
        if(selectedStyle.hasOwnProperty('fontFamily')) {
            optionListComponent.push(
                <OptionFontFamily 
                    key={2} 
                />
            );
        }
        if(selectedStyle.hasOwnProperty('fillColor')) {
            optionListComponent.push(
                <OptionFillColor 
                    key={3} 
                />
            );
        }
        return optionListComponent;
    }
}
export default OptionFactory;