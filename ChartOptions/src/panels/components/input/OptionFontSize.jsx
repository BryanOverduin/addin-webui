import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateCurrentTab, updateVotesText } from '../../../actions/tabsActions';
import Slider, { Handle } from 'rc-slider';
import Tooltip from 'rc-tooltip';
import 'rc-slider/assets/index.css';


class OptionFontSize extends Component {

    updateFontSize(value) {
        let newTab = this.props.selectedTab;
        let style = this.props.selectedStyleName;
        if(!this.votesTextSelected(value)) {
            this.props.dispatch(updateCurrentTab({
                ...newTab,
                [style]: {
                    ...newTab[style],
                    fontSize: value
                }
            }));
        }
    }

    votesTextSelected(fontSize) {
        if(this.props.selectedStyleName === 'votesText') {
            let votesText = this.props.votesText;
            this.props.dispatch(updateVotesText({
                ...votesText,
                fontSize: fontSize
            }))
            return true;
        }
        return false;
    }

    getHandle(props, fontSize) {
        delete props.dragging;

        return (
            <Tooltip
                prefixCls="rc-slider-tooltip"
                overlay={fontSize}
                placement="top"
                key={props.index}
            >  
                <Handle
                    value={props.value} {...props}
                />
            </Tooltip>
        )
    }

    render() {
        let style = this.props.selectedTab[this.props.selectedStyleName]        
        if(this.props.selectedStyleName === 'votesText') {
            style = this.props.votesText
        }
        let fontSize = style.fontSize;
        return (
            <div className="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                <div className="form-group">
                    <label>Font size</label>
                    <Slider min={20} max={50} value={fontSize} onChange={this.updateFontSize.bind(this)} handle={(props) => this.getHandle(props, fontSize)} />
                </div>
            </div>
        )
    }
}
export default connect(
    (state) => {
        return {
            selectedStyleName: state.tabsReducer.selectedStyleName,
            selectedTab: state.tabsReducer.selectedTab,
            votesText: state.tabsReducer.votesText
        }
    }
)(OptionFontSize);