import React, { Component } from 'react';
import { connect } from 'react-redux';
import { updateCurrentTab, updateVotesText } from '../../../actions/tabsActions';

class OptionFontFamily extends Component {

    updateFontFamily(e) {
        console.log('newFont', e.target.value);
        let newTab = this.props.selectedTab;
        let style = this.props.selectedStyleName;
        if(!this.votesTextSelected(e.target.value)) {
            this.props.dispatch(updateCurrentTab({
                ...newTab,
                [style]: {
                    ...newTab[style],
                    fontFamily: e.target.value
                }
            }));
        }
    }

    votesTextSelected(fontFamily) {
        if(this.props.selectedStyleName === 'votesText') {
            let votesText = this.props.votesText;
            this.props.dispatch(updateVotesText({
                ...votesText,
                fontFamily: fontFamily
            }))
            return true;
        }
        return false;
    }

    render() {
        let style = this.props.selectedTab[this.props.selectedStyleName]
        if(this.props.selectedStyleName === 'votesText') {
            style = this.props.votesText
        }
        let fontFamily = style.fontFamily;
        let fontTypes = [
            // ClassName / Font Family
            ['calibri', 'Calibri'],
            ['arial', 'Arial'],
            ['times', 'Times New Roman'],
            ['courier', 'Courier New']         
        ];
        return (
            <div className="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                <div className="form-group">
                    <label>Font Family</label>
                    <select value={fontFamily} onChange={this.updateFontFamily.bind(this)}  className="form-control font-family input-lg"  placeholder="Font family" type="text">
                        <option className="font ">Select...</option>
                            {fontTypes.map(fontType => {
                                return (
                                    <option 
                                        key={fontType[0]}
                                        selected={fontFamily === fontType[1]? 'selected' : null} 
                                        className={"font " + fontType[0]}>
                                        {fontType[1]}
                                    </option>
                                )
                            })}
                    </select>
                </div>
            </div>
        )
    }
}
export default connect(
    (state) => {
        return {
            selectedTab: state.tabsReducer.selectedTab,
            selectedStyleName: state.tabsReducer.selectedStyleName,
            votesText: state.tabsReducer.votesText
        }
    }
)(OptionFontFamily);