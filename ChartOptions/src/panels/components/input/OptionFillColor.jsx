import React, { Component } from 'react';
import { connect } from 'react-redux';
import { generateColorList } from '../helpers/ColorHelper';
import { updateCurrentTab } from '../../../actions/tabsActions';
import { TwitterPicker, HuePicker } from 'react-color';

class OptionFillColor extends Component {

    updateFillColor(color, e) {
        let newTab = this.props.selectedTab;
        let style = this.props.selectedStyleName;

        newTab = {
            ...newTab,
            [style]: {
                ...newTab[style],
                fillColor: color.hex
            }
        }
        this.props.dispatch(updateCurrentTab(newTab));
    }

    render() {
        let style = this.props.selectedTab[this.props.selectedStyleName];
        let fillColor = style.fillColor;
        return (
            <div className="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                <div className="form-group">
                    <label>Fill color</label>
                    <HuePicker  onChange={this.updateFillColor.bind(this)} color={fillColor}/>
                    <TwitterPicker triangle={"hide"} className="color-picker twitter" colors={generateColorList(fillColor)} color={fillColor} onChange={this.updateFillColor.bind(this)}/>                    
                </div>
            </div>
        )
    }
}
export default connect(
    (state) => {
        return {
            selectedTab: state.tabsReducer.selectedTab,
            selectedStyleName: state.tabsReducer.selectedStyleName
        }
    }
)(OptionFillColor);