import React, { Component } from 'react';
import { TwitterPicker, HuePicker } from 'react-color';
import { generateColorList } from '../helpers/ColorHelper';
import { updateCurrentTab, updateVotesText } from '../../../actions/tabsActions';
import { connect } from 'react-redux';

class OptionFontColor extends Component {

    updateFontColor(color, e) {
        let newTab = this.props.selectedTab;
        let style = this.props.selectedStyleName;
        if(!this.votesTextSelected(color.hex)) {
            this.props.dispatch(updateCurrentTab({
                ...newTab,
                [style]: {
                    ...newTab[style],
                    fontColor: color.hex
                }
            }));
        }
    }
    
    votesTextSelected(fontColor) {
        if(this.props.selectedStyleName === 'votesText') {
            let votesText = this.props.votesText;
            this.props.dispatch(updateVotesText({
                ...votesText,
                fontColor: fontColor
            }))
            return true;
        }
        return false;
    }

    render() {
        let style = this.props.selectedTab[this.props.selectedStyleName]
        if(this.props.selectedStyleName === 'votesText') {
            style = this.props.votesText
        }
        let fontColor = style.fontColor;
        return (
            <div className="col-md-4 col-sm-4 col-xs-4 col-lg-4">
                <div className="form-group">
                    <label>Font color</label>
                    <HuePicker  onChange={this.updateFontColor.bind(this)} color={fontColor}/>
                    {/* <SliderPicker className="color-picker hue" onChange={this.updateFontColor.bind(this)} color={this.props.fontColor}/> */}
                    <TwitterPicker triangle={"hide"} className="color-picker twitter" colors={generateColorList(fontColor)}  color={fontColor} onChange={this.updateFontColor.bind(this)}/>                    
                
                </div>
            </div>
        )
    }
}
export default connect(
    (state) => {
        return {
            selectedStyleName: state.tabsReducer.selectedStyleName,
            selectedTab: state.tabsReducer.selectedTab,
            votesText: state.tabsReducer.votesText
        }
    }
)(OptionFontColor);