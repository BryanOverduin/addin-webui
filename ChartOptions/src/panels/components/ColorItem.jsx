import React from 'react';
import OptionFactory from './factory/OptionFactory';

class ColorItem extends OptionFactory {

    render() {
        if(!this.props.selectedStyle) {
            return (
                <div className={"col-md-12 col-lg-12 col-xs-12 col-sm-12 full-height padding-0-10 text-center"}>
                    <div className={"full-height vertical-align-wrap height-100px"}>
                        <div className="vertical-align vertical-align--middle select-component">
                            <h3>Select a component from below to customize.</h3>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div>
                {this.getOptions(this.props.selectedStyle)}
            </div>
        )
    }
}
export default ColorItem;