import React, { Component } from 'react';
import { connect } from 'react-redux';
import { selectTab } from '../../../actions/tabsActions';

class TabsSelect extends Component {

    selectTab(tabId) {
        let index = this.props.selectedTab.index;
        if(index !== tabId) {
            this.props.dispatch(selectTab(tabId));
        }
    }

    render() {
        return (
            <ul className="nav nav-tabs">
                {this.props.allTabs.map(tabItem => {
                    const active = this.props.selectedTab.index === tabItem.index ? 'active' : '';
                    return (
                        <li className={active} onClick={() => this.selectTab(tabItem.index)} key={tabItem.index} ><a>{tabItem.index}</a></li>
                    )
                })}
            </ul>
        )
    }
}
export default connect(
    (state) => {
        return {
            allTabs: state.tabsReducer.allTabs,
            selectedTab: state.tabsReducer.selectedTab,
        }
    }
)(TabsSelect);