import tabsReducer from '../reducers/tabsReducer';
import { createStore, combineReducers } from 'redux';

const rootReducer = combineReducers({
    tabsReducer: tabsReducer
});

const initialState = {
    // tabsReducer
    tabsReducer: {
        allTabs: [],
        selectedTab: null,
        votesText: {},
        randomPercentage: Math.floor(Math.random() * 100) + '%'
    }
}

const store = createStore(rootReducer, initialState);

export default store;