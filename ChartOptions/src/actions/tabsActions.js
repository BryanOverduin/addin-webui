export function addTab(newTab) {
    return {
        type: 'ADD_TAB',
        newTab,
        selectedTab: newTab
    }
}

export function updateVotesText(votesText) {
    return {
        type: 'UPDATE_VOTES_TEXT',
        votesText
    }
}
export function updateCurrentTab(newTabSettings) {
    return {
        type: 'UPDATE_TAB',
        newTabSettings
    }
}

export function loadSettings(initialSettings) {
    return {
        type: 'SET_SETTINGS',
        initialSettings
    }
}

export function selectTab(tabId) {
    return {
        type: 'SELECT_TAB',
        tabId
    }
}

export function selectStyle(selectedStyle, selectedStyleName) {
    return {
        type: 'SELECT_STYLE',
        selectedStyle,
        selectedStyleName
    }
}
