# Addin Web UI

Context: (https://sendsteps.atlassian.net/browse/AI-33)

## Usage

1. 	Navigate to `root/ChartOptions`
2.	`npm install` to install dependencies
3.	`npm start` to start the development server
4.	Navigate to `localhost:3000`

## Explanation

The panel expects a JSON object containing styling variables. Using the global `drawHtml()` function the styles can be imported into the panel and modified.
Clicking the "Save Settings" button will trigger an `about:save` event and will load the new Stringified JSON into a hidden `#jsonTag` element.

Json Format:
```
{
    settings: [{
        index: 'A',
        answerStyle: {
            fontFamily: 'Times new Roman',
            fontSize: 20,
            fontColor: '#000000'  
        },
        bulletStyle: {
            fontFamily: 'Times new Roman',
            fontSize: 20,
            fontColor: '#000000'  
        },
        percentageStyle: {
            fontFamily: 'Times new Roman',
            fontSize: 20,
            fontColor: '#000000' 
        },
        barStyle: {
            fillColor: '#000000'
        }
    }],
    votesText: {
        fontFamily: 'Times new Roman',
        fontSize: 20,
        fontColor: '#000000' 
    }
}
```